The site features coupons for over 500 online retail brands, across 14 categories. These include all the major retail sites right from Flipkart, eBay, HomeShop18, India times, Myntra, Pepperfry, Yebhi, to name a few. Looking for coupons on the site is pretty straightforward as it has been divided into several broad categories. You can browse through ‘Top Coupon’, ‘New Coupons’, ‘Top Deals’ and ‘Expiring Coupons’. Alternatively, if you are looking for coupons for a particular product, then you can browse for coupons based on product like cameras, tablets, books, flights, mobile, hotel, furniture etc. You can also browse based on categories.

Visit Our Site 


PRODUCT DESCRIPTION
UNIQUE FEATURES:
Customer Support
SEO Friendly Versions
Speed optimized Software                
Stand Your Business Brand
Easy script installation                      
User Friendly Scripts
Dedicated Support Team                  
Easy Customization

General Features:
Support different account views
Information broad casting
Easy Work flow of buying a deal
Best Backend features

Front-end:
Login with google/facebook/yahoo
Profile
Multi-level category.
Unlimited deal address, location, Contacts, redeem timing, deal nearest address to find out the deal location.
Unlimited deal image upload.
Unlimited deal Rate card upload.
Check Pending, Active, Rejected, Expired Deals.
Reactivate the expired deals.
Featured seller deals.
Check billing history, sell list, and reviews.
Edit active deals.
Search through category and subcategory listing pages.
Search by minimum price and maximum price.
Search by location and km range.
View the rate cards of deals.
Rate your Deals and add to cart.
Write a review on deals
Give feedback to the seller.
View the location of the deal through Google map.
See the similar deals in your selected city.
Check discounts
Payment Gateway Enabled

Admin:
Complete CMS system.
List of users/sellers.
See user details/seller details
Suspend users, seller
Generate/edit coupon for discount.
Check all pending,active,rejected,expired deals,
Approve pending deals /edited deals
Option to Edit/Delete deals
You can Add/edit/delete category subcategory.
You can Add/edit/delete banner ad, feature ads.
You can Add/edit/delete payment gateways
Option to Check Paid/Unpaid/purchased orders.
Option to Check billing history of seller and user.
Check reviews.
Coupon code generation module for admin panel, where we can generate some coupon code to offer additional discount for special occasions.
Vendor list — Sort by name, sort by categories, sort by city, sort by expiry date, sort by featured.
Featured deal is per week basis.
Admin must be able to see user details (both customer and vendor separately), Details include userid/email, previous orders.
Option to reset customer password.
Option to give and subtract some credit from seller account

Other features:
Integrated Google Maps
Show your latest Twitter posts
Google Analytics integration
Facebook Connect
SSL support
Buy coupon for a friend


Check out products:

https://www.doditsolutions.com/

http://scriptstore.in/

http://phpreadymadescripts.com/
